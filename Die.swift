//
//  Die.swift
//  hi-assign7
//
//  Created by Student on 2016-11-09.
//  Copyright © 2016 Student. All rights reserved.
//
//  Project:      assign-7-2016
//  File:         Die.swift
//  Author:       Ismael F. Hepp
//  Date:         Nov 15, 2016
//  Course:       IMG204
//
//  You are to develop a Single View iOS application that should satisfy the
//  following requirements:
//
//  1 - Look at the very incomplete exploratory app (zip file) available at
//  /img204/solutions/Assignment7 .
//  2 - he application is a game to guess a sum of two dice. You play against
//  the computer.
//  3 - Use labels and text fields to show the sum you guess. After you make
//  your guess, the computer will show its own guess (it must be different from
//  yours).
//  4 - Click on a Play button to roll both dice (use random number generator).
//  The result of the roll should be displayed using both images and the number
//  representing their sum. A point is scored if you or the computer correctly
//  predicted the result.
//  5 - Repeat step 4 as many times as you like.
//  6 - At any moment you can repeat step 3.
//  7 - There should be set of labels to display how many times both you and
//  the computer guessed correctly the result.
//  8 - Use StackView to simplify the layout of your app.
//  9 - Create a group called Model.
//  10 - Inside the Model group, create necessary classes to play the game
//  (your previous assignment dealing with dice should help) and to maintain
//  the score.
//

import Foundation

class Die {
  private var value: Int
  
  public init() {
    value = Int(arc4random_uniform(6) + 1 )
  }
  
  public func roll() {
    value = Int(arc4random_uniform(6) + 1 )
  }
  
  public func showFaceValue() -> Int {
    return value
  }
  
  public var description: String {
    return "A die with face value of \(value)."
  }
  
}
