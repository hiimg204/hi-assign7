//
//  PairOfDice.swift
//  hi-assign7
//
//  Created by Student on 2016-11-09.
//  Copyright © 2016 Student. All rights reserved.
//
//  Project:      assign-7-2016
//  File:         PairOfDice.swift
//  Author:       Ismael F. Hepp
//  Date:         Nov 15, 2016
//  Course:       IMG204
//
//  You are to develop a Single View iOS application that should satisfy the
//  following requirements:
//
//  1 - Look at the very incomplete exploratory app (zip file) available at
//  /img204/solutions/Assignment7 .
//  2 - he application is a game to guess a sum of two dice. You play against
//  the computer.
//  3 - Use labels and text fields to show the sum you guess. After you make
//  your guess, the computer will show its own guess (it must be different from
//  yours).
//  4 - Click on a Play button to roll both dice (use random number generator).
//  The result of the roll should be displayed using both images and the number
//  representing their sum. A point is scored if you or the computer correctly
//  predicted the result.
//  5 - Repeat step 4 as many times as you like.
//  6 - At any moment you can repeat step 3.
//  7 - There should be set of labels to display how many times both you and
//  the computer guessed correctly the result.
//  8 - Use StackView to simplify the layout of your app.
//  9 - Create a group called Model.
//  10 - Inside the Model group, create necessary classes to play the game
//  (your previous assignment dealing with dice should help) and to maintain
//  the score.
//

import Foundation

class PairOfDice {
  private var die1: Die
  private var die2: Die
  public var sumOfDice: Int {
    get {
      return ( die1.showFaceValue() + die2.showFaceValue() )
    }
  }
  
  public init() {
    die1 = Die()
    die2 = Die()
  }
  
  public func rollDice() {
    die1.roll()
    die2.roll()
  }
  
  public func showDie1() -> Int {
    return die1.showFaceValue()
  }
  
  public func showDie2() -> Int {
    return die2.showFaceValue()
  }
  
  public var description: String {
    var message = "A die with face value of \(die1.showFaceValue())"
    message += " and another die with face value of \(die2.showFaceValue())."
    return message
  }
}
